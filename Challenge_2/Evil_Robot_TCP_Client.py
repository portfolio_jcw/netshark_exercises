# Simple TCP Client for simulating a Robot who wants to take over the world!!!

import socket

target_host = "192.168.0.102"   # coded for other laptop
target_port = 9999

# create a socket object

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connect the client

client.connect((target_host, target_port))

# send some data

client.send(b"Hello there!")

# receive some data

response = client.recv(4096)

print(response)